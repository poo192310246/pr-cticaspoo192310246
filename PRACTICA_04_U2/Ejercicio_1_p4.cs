﻿using System;

namespace Ejercicio_1_p4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Suma(19, 45));
            Console.WriteLine(Suma(1, 5, 78));
            Console.WriteLine(Suma(15, 30, 101, 158));
            Console.ReadKey();
        }
        public static int Suma(int x, int y)
        {
            return x + y;
        }
        public static int Suma(int x, int y, int w)
        {
            return x + y + w;
        }
        public static int Suma(int x, int y, int w, int z)
        {
            return x + y + w + z;
        }

    }
}
