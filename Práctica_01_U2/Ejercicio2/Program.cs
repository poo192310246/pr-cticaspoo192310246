﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ejercicio2
{
    public class Operacion
    {
        public int m;
        public int n;
        public int[,] mat;
        public int ImprimeMatriz()
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.Write(" " + mat[i, j]);
                }
                Console.WriteLine();
            }
        }

        public int leerMatriz()
        {
            Console.Write("numero de filas:");
            n = int.Parse(Console.ReadLine());
            Console.Write("numero de columnas:");
            m = int.Parse(Console.ReadLine());
            mat = new int[n, m];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.Write("elementos de la matriz[" + i + "," + j + "]=");
                    mat[i, j] = int.Parse(Console.ReadLine());
                }
            }
        }

        static void Main(string[] args)
        {
            Operacion o = new Operacion();
            o.ImprimeMatriz();
            Console.WriteLine(o.ImprimeMatriz());
            Console.ReadKey();

            o.leerMatriz();
            Console.WriteLine(o.leerMatriz());



        }
    }
}